# Ozyegin University Distributed Systems and Cloud Computing Term Project
## Topic: 
Developing a web service for viewing 3D printed models and visualizing the deformations
on a web browser.
## Background Information:
- While something is being 3D printed, due to material shrinkage, warping  occurs.
- A blue light scanner's software is used to combine multiple scanned images of a
3D printed object into a 3D surface image that can be stored, rotated etc.
- The Visualization Toolkit (VTK) is an open-source software system for 3D computer graphics,
image processing and visualization.

## Further Work:
I will host a static website on AWS, use the JavaScript
library, VTK.js to visualize the deformations and combine the output of the blue light
scanner with the expected 3D model and finally visualize the deformations to the users
of the service.
## Outcomes:
- Hands on experience with Docker
- Industry 4.0
- Using an open source library
- Using cloud for compute intensive scientific calculations